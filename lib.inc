section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    xor rax, rax
.count:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .count
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rax, 1
    mov rdi, 1
    mov rsi, 0xA
    mov rdx, 1
    syscall
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    push r12
    push r13
    mov r12, rsp
    mov r13, 10
    mov rax, rdi
.loop:
    dec rsp
    xor rdx, rdx
    div  r13
    add rdx, '0'
    mov  byte[rsp], dl
    test rax, rax
    jz .print
    jmp .loop
.print:
    mov rdi, rsp
    call print_string
    mov rsp, r12
    pop r13
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rdi
    cmp rdi, 0
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    call print_uint
    pop rdi
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    push rbx
.loop:
    mov bl, byte [rsi + rax]
    cmp byte [rdi + rax], bl
    jne .err
    cmp byte [rdi + rax], 0
    je .equals
    inc rax
    jmp .loop
.err:
    xor rax, rax
    jmp .end
.equals:
    mov rax, 1
.end:
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    dec rsp
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    je .str_end
    xor rax, rax
    mov al, byte [rsp]
    jmp .end
.str_end:
    xor rax, rax
.end:
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
push rdi
push rsi
.whitespaces:
    call read_char
    cmp al, ' '
    je .whitespaces
    cmp al, 0x9
    je .whitespaces
    cmp al, 0xa
    je .whitespaces
pop r11
pop r10
push r15
xor r15, r15
.loop:
    cmp r11, r15
    je .fail
    mov byte[r10+r15], al
    cmp al, ' '
    je .succ
    cmp al, 0x9
    je .succ
    cmp al, 0xa
    je .succ
    cmp al, 0
    je .succ
    inc r15
    push r10
    push r11
    call read_char
    pop r11
    pop r10
    jmp .loop
.fail:
    pop r15
    xor rax, rax
    jmp .end
.succ:
    mov byte[r10+r15], 0
    mov rax, r10
    mov rdx, r15
    pop r15
    jmp .end
.end:
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rcx, rcx
    xor rdx, rdx
    mov r10, 10
.loop:
    mov sil, [rdi+rcx]
    cmp sil, '0'
    jl .return
    cmp sil, '9'
    jg .return
    inc rcx
    sub sil, '0'
    mul r10
    add rax, rsi
    jmp .loop
.return:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
xor rax, rax
    cmp byte [rdi], '-'
    jne parse_uint
parse:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .return
    neg rax
    inc rdx
.return:
    ret




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rbx
    xor rax, rax
.loop:
    cmp rax, rdx
    je .err
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0
    je .end
    inc rax
    jmp .loop
.err:
    xor rax, rax
.end:
    pop rbx
    ret
